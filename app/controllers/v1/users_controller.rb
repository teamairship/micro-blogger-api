# frozen_string_literal: true

module V1
  class UsersController < ApplicationController
    before_action :set_user, only: %i[show update destroy]

    def index
      @users = policy_scope User.all
    end

    def show; end

    def create
      @user = User.new(user_params)
      authorize @user

      if @user.save
        render :show, status: :created, location: v1_users_url(@user)
      else
        render json: @user.errors, status: :unprocessable_entity
      end
    end

    def update
      if @user.update(user_params)
        render :show, status: :ok, location: v1_users_url(@user)
      else
        render json: @user.errors, status: :unprocessable_entity
      end
    end

    def destroy
      @user.destroy
    end

    private

      def set_user
        @user = User.find(params[:id])
        authorize @user
      end

      def user_params
        params.require(:user).permit(policy(User).permitted_attributes)
      end
  end
end
