# frozen_string_literal: true

module V1
  class AuthenticationController < ApplicationController
    skip_after_action :verify_authorized
    skip_after_action :verify_policy_scoped

    def authenticate
      command = AuthenticateUser.new(auth_params)
      @token = command.perform
      @user = command.user
      render "v1/authentication/authenticate" and return unless @token.nil?

      render json: command.errors, status: :unauthorized
    end

    private

      def auth_params
        params.permit(:email, :password)
      end
  end
end
