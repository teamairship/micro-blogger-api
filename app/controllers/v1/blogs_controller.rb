# frozen_string_literal: true

module V1
  class BlogsController < ApplicationController
    before_action :set_blog, only: %i[show update destroy]

    def index
      @blogs = policy_scope Blog.all
    end

    def show; end

    def create
      @blog = Blog.new(blog_params)
      @blog.user_id = current_user.id

      authorize @blog

      if @blog.save
        render :show, status: :created, location: v1_blogs_url(@blog)
      else
        render json: @blog.errors, status: :unprocessable_entity
      end
    end

    def update
      if @blog.update(blog_params)
        render :show, status: :ok, location: v1_blogs_url(@blog)
      else
        render json: @blog.errors, status: :unprocessable_entity
      end
    end

    def destroy
      @blog.destroy
    end

    private

      def set_blog
        @blog = Blog.find(params[:id])
        authorize @blog
      end

      def blog_params
        params.require(:blog).permit(policy(Blog).permitted_attributes)
      end
  end
end
