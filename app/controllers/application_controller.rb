# frozen_string_literal: true

class ApplicationController < ActionController::API
  include Pundit

  before_action :authenticate_request
  after_action :verify_authorized, except: :index
  after_action :verify_policy_scoped, only: :index

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  def index; end

  private

    def current_user
      @current_user ||= authenticate_request
    end

    def authenticate_request
      return nil if request.authorization.blank?

      @authenticate_request ||= AuthorizeRequest.new(request.headers).perform
    end

    def user_not_authorized
      render \
        json: { authorization: ["You are not authorized to perform this action."] },
        status: :unauthorized
    end
end
