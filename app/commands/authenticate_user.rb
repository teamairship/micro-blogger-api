# frozen_string_literal: true

class AuthenticateUser < Imperator::Command
  include ActiveModel::Validations

  string :email
  string :password

  validates :email,    presence: true
  validates :password, presence: true

  def action
    JsonWebToken.encode(user_id: user.id) if user
  end

  def user
    user = @user ||= User.find_by(email: @email)
    return user if user&.authenticate(@password)

    errors.add :user_authentication, 'invalid credentials'
    nil
  end
end
