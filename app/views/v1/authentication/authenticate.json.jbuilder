# frozen_string_literal: true

json.auth_token @token
json.user @user, partial: "v1/users/user", as: :user
