# frozen_string_literal: true

json.array! @blogs, partial: 'v1/blogs/blog', as: :blog
