# frozen_string_literal: true

json.url v1_blog_url(blog, format: :json)

json.extract! blog,
              :title,
              :article,
              :published_date,
              :id

json.author do
  json.name blog.author.display_name
  json.url  v1_user_url(blog.author, format: :json)
end
