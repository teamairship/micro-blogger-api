# frozen_string_literal: true

json.partial! "v1/blogs/blog", blog: @blog
