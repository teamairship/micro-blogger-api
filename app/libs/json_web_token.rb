# frozen_string_literal: true

class JsonWebToken
  class << self
    def encode(payload, exp = 24.hours.from_now)
      payload[:exp] = exp.to_i
      JWT.encode(payload, ENV['jwt'])
    end

    def decode(token)
      body = JWT.decode(token, ENV['jwt'])[0]
      HashWithIndifferentAccess.new body
    end
  end
end
