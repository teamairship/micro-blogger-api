# MicroBlogger

An API for a micro blogging platform

* Guests can view published content and author list
* Authors can edit their profile & blogs
* Admins can view the list of authors and edit any blog

## Tasks

### User Bios

As an author, I want to be able to write a short bio that
is displayed with each of my postings.

### BUG: Publishing

It has been reported that authors are not able to publish
their blogs

### Moderation

As an admin, I want to be able to put a blog in a state
of moderation that also unpublishes it.

Once a blog post has this state, the author must edit and
submit for another review before an admin will republish.

### Categories

As an author, I would like to be able to assign a category
to my posting from an existing list of options.

As an admin, I can manage the list of categories available
to the authors.

### Registration

As a guest, I should be able to register so that I may start
producing content.

### Author Titles

As a user, when I look up an author, I should also get a list
of titles and links to her published articles.
