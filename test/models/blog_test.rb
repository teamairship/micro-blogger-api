# frozen_string_literal: true

require 'test_helper'

class BlogTest < ActiveSupport::TestCase
  test 'has author' do
    blog = blogs(:author1)

    assert blog.author.present?
  end
end
