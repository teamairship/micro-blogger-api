# frozen_string_literal: true

require 'test_helper'

class AuthenticationControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:admin)
  end

  test "should return token" do
    post v1_authenticate_url, params: { email: @user.email, password: 'password' }
    assert_response :success
  end

  test "should fail auth" do
    post v1_authenticate_url, params: { email: @user.email, password: 'BAD PASSWORD' }
    assert_response :unauthorized
  end
end
