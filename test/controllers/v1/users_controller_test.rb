# frozen_string_literal: true

require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  test "admin can list users" do
    get v1_users_url, headers: auth_headers(users(:admin))

    assert_response :success
  end

  test "admin can create user" do
    assert_difference('User.count') do
      post v1_users_url, params: { user: {
        display_name: 'some user',
        email: 'new.user@mailinator.com',
        password: 'password',
        password_confirmation: 'password'
      } }, headers: auth_headers(users(:admin))
    end

    assert_response :created
  end

  test "admin can view users" do
    get v1_user_url(users(:admin)), headers: auth_headers(users(:admin))

    assert_response :success
  end

  test "admin can update user" do
    patch v1_user_url(users(:admin)), params: { user: {
      display_name: 'I am admin'
    } }, headers: auth_headers(users(:admin))

    assert_response :ok
  end

  test "admin can destroy user" do
    assert_difference('User.count', -1) do
      delete v1_user_url(users(:admin)), headers: auth_headers(users(:admin))
    end

    assert_response :no_content
  end

  test "author can view herself" do
    get v1_user_url(users(:author)), headers: auth_headers(users(:author))

    assert_response :success
  end

  test "author can update herself" do
    patch v1_user_url(users(:author)), params: { user: {
      display_name: 'I am author!'
    } }, headers: auth_headers(users(:author))

    assert_response :ok
  end

  test "sally CANNOT update phil" do
    patch v1_user_url(users(:author)), params: { user: {
      display_name: 'I am author!'
    } }, headers: auth_headers(users(:sally))

    assert_response :unauthorized
  end
end
