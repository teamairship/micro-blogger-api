# frozen_string_literal: true

module AuthTestHelper
  def user_token user
    AuthenticateUser.new(email: user.email, password: 'password').perform
  end

  def auth_headers user, headers = {}
    headers["HTTP_AUTHORIZATION"] = user_token(user)
    headers
  end
end
