# frozen_string_literal: true

Rails.application.routes.draw do
  concern :api_base do
    post 'authenticate', to: 'authentication#authenticate', as: :authenticate
    resources :users
    resources :blogs
  end

  namespace :v1, defaults: { format: :json } do
    concerns :api_base
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
